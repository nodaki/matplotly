#! env python
# -*- coding: utf-8 -*-
# Date: 2019/04/19
# Filename: pyplot

from itertools import product
from typing import List

import numpy as np
import plotly
import plotly.graph_objs as go

from matplotly import normalize_kwargs

# Activate offline mode
plotly.offline.init_notebook_mode(connected=False)


class FigureManager(object):
    def __init__(self):
        self.figures = []
        self.axes = []

    def gcf(self):
        """
        Get current figure.

        Returns:
            Current figure instance.

        """
        if not self.figures:
            self.new_figure()
        return self.figures[-1]

    def gca(self):
        """
        Get current axes instance.

        Returns:
            Current axes instance.

        """
        return self.axes[-1][0]

    def new_figure(self,
                   rows: int = 1,
                   cols: int = 1) -> None:
        """
        Create new figure.

        Args:
            rows: # of rows.
            cols: # of cols.

        """
        fig = plotly.tools.make_subplots(rows=rows, cols=cols)
        self.figures.append(fig)
        self.axes.append([Axes(fig, row, col) for row, col in product(range(1, rows + 1), range(1, cols + 1))])


figManager = FigureManager()


def gcf():
    return figManager.gcf()


def gca():
    return figManager.gca()


class Axes(object):
    def __init__(self,
                 fig: go.Figure,
                 row: int,
                 col: int) -> None:
        self.fig = fig
        self.row = row
        self.col = col

    def plot(self, *args, **kwargs):
        """
        Plot function.

        Examples:
            Plot y versus x as lines and/or markers.

            The coordinates of the points or line nodes are given by *x*, *y*.

            The optional parameter *fmt* is a convenient way for defining basic
            formatting like color, marker and linestyle. It's a shortcut string
            notation described in the *Notes* section below.

            plot(x, y)        # plot x and y using default line style and color
            plot(x, y, 'bo')  # plot x and y using blue circle markers
            plot(y)           # plot y using x as index array 0..N-1
            plot(y, 'r+')     # ditto, but with red plusses

            You can use `.Line2D` properties as keyword arguments for more
            control on the appearance. Line properties and *fmt* can be mixed.
            The following two calls yield identical results:

            plot(x, y, 'go--', linewidth=2, markersize=12)
            plot(x, y, color='green', marker='o', linestyle='dashed',
                 linewidth=2, markersize=12)

        """
        tmp = []
        data = []
        for arg in args:
            if type(arg) == str:
                plotly_kwargs = normalize_kwargs(arg, **kwargs)
                data.extend(generate_traces(tmp, plotly_kwargs))
                tmp = []
            else:
                tmp.append(arg)
        plotly_kwargs = normalize_kwargs([], **kwargs)
        data.extend(generate_traces(tmp, plotly_kwargs))

        for trace in data:
            self.fig.add_trace(trace, self.row, self.col)


def split_list(l: list,
               n: int) -> list:
    """
    Split the list into N elements.

    Args:
        l: Original list.
        n: N elements.

    Returns:
        List of N elements.

    """
    for idx in range(0, len(l), n):
        yield l[idx:idx + n]


def generate_traces(tmp: list,
                    plotly_kwargs: dict) -> List[go.Scatter]:
    """
    Generate traces from input args and kwargs.

    Args:
        tmp: tmp results.
        plotly_kwargs: kwargs for plotly.

    Returns:
        data for plot.

    """
    data = []
    split_result = split_list(tmp, n=2)
    for s in split_result:
        if len(s) == 0:
            raise ValueError("Value error")
        elif len(s) == 1:
            x = np.arange(len(s[0]))
            y = s[0]
            data.append(go.Scatter(x=x, y=y, **plotly_kwargs))
        elif len(s) == 2:
            x = s[0]
            y = s[1]
            data.append(go.Scatter(x=x, y=y, **plotly_kwargs))
    return data


def plot(*args, **kwargs):
    return gca().plot(*args, **kwargs)


def figure(**kwargs):
    figManager.new_figure(**kwargs)
    return figManager.figures[-1]


def show(inline: bool = True) -> None:
    """
    Show figure object.

    Args:
        inline: If true, plot inline, else he browser is launched and displayed there.

    """
    if inline:
        plotly.offline.iplot(gcf())
    else:
        plotly.offline.plot(gcf())
