__version__ = "1.0.0"

from typing import Tuple

from plotly.colors import DEFAULT_PLOTLY_COLORS

format_string_alias = dict(color=["b", "g", "r", "c", "m", "y", "k", "w"],
                           markers=[".", ",", "o", "v", "^", "<", ">", "1", "2", "3", "4",
                                    "s", "p", "*", "h", "H", "+", "x", "D", "d", "|", "_"],
                           line_style=["-", "--", "-.", ":"])


def convert_color_format(color_property: list) -> str:
    """
    Convert matplotlib color format string to plotly color format.

    Args:
        color_property:　Color property.

    Returns:
        color: plotly color format.

    """
    # Default color
    color = DEFAULT_PLOTLY_COLORS[0]
    if "b" in color_property:
        color = DEFAULT_PLOTLY_COLORS[0]
    elif "g" in color_property:
        color = DEFAULT_PLOTLY_COLORS[2]
    elif "r" in color_property:
        color = DEFAULT_PLOTLY_COLORS[1]
    elif "c" in color_property:
        color = DEFAULT_PLOTLY_COLORS[9]
    elif "m" in color_property:
        color = DEFAULT_PLOTLY_COLORS[6]
    elif "y" in color_property:
        color = DEFAULT_PLOTLY_COLORS[8]
    elif "k" in color_property:
        color = DEFAULT_PLOTLY_COLORS[7]
    elif "w" in color_property:
        color = "rgb(250, 250, 250)"
    return color


def convert_marker_format(marker_property: list) -> dict:
    """
    Convert matplotlib marker format string to plotly marker format.

    Args:
        marker_property: marker property

    Returns:
        marker: plotly marker format.

    """
    marker = {}
    if "." in marker_property:
        marker = dict(symbol="circle")
    elif "," in marker_property:
        marker = dict(symbol="hash")
    elif "o" in marker_property:
        marker = dict(symbol="circle", size=10)
    elif "v" in marker_property:
        marker = dict(symbol="triangle-down")
    elif "^" in marker_property:
        marker = dict(symbol="triangle-up")
    elif ">" in marker_property:
        marker = dict(symbol="triangle-right")
    elif "<" in marker_property:
        marker = dict(symbol="triangle-left")
    elif "1" in marker_property:
        marker = dict(symbol="triangle-up-open")
    elif "2" in marker_property:
        marker = dict(symbol="triangle-down-open")
    elif "3" in marker_property:
        marker = dict(symbol="triangle-left-open")
    elif "4" in marker_property:
        marker = dict(symbol="triangle-right-open")
    elif "s" in marker_property:
        marker = dict(symbol="square")
    elif "p" in marker_property:
        marker = dict(symbol="pentagon")
    elif "*" in marker_property:
        marker = dict(symbol="star")
    elif "*" in marker_property:
        marker = dict(symbol="star")
    elif "h" in marker_property:
        marker = dict(symbol="hexagon")
    elif "H" in marker_property:
        marker = dict(symbol="hexagon2")
    elif "+" in marker_property:
        marker = dict(symbol="cross")
    elif "x" in marker_property:
        marker = dict(symbol="x")
    elif "D" in marker_property:
        marker = dict(symbol="diamond")
    elif "d" in marker_property:
        marker = dict(symbol="diamond-tall")
    elif "|" in marker_property:
        marker = dict(symbol="line-ns")
    elif "_" in marker_property:
        marker = dict(symbol="line-ew")
    return marker


def convert_line_format(line_style_property: list) -> dict:
    """
    Convert matplotlib line string format to plotly line format.

    Args:
        line_style_property: line property.

    Returns:
        plotly line format.

    """
    line = {}
    if "--" in line_style_property:
        line = dict(dash="dash")
    elif "-." in line_style_property:
        line = dict(dash="dashdot")
    elif ":" in line_style_property:
        line = dict(dash="dot")
    elif "-" in line_style_property:
        line = dict(dash="solid")
    return line


def convert_format_string_to_line2d_properties(format_string: str) -> Tuple[str, dict, dict]:
    """
    Convert format string to plotly line2d property.

    Args:
        format_string: Input format string.

    Returns:
        mode, line, marker

    """
    # Color property
    color_property = [c for c in format_string_alias["color"] if c in format_string]
    # Marker property
    marker_property = [m for m in format_string_alias["markers"] if m in format_string]
    # Line style property
    line_style_property = [l for l in format_string_alias["line_style"] if l in format_string]

    color = convert_color_format(color_property)
    marker = convert_marker_format(marker_property)
    line = convert_line_format(line_style_property)

    mode = "lines"
    if marker and line:
        mode = "lines+markers"
        line["color"] = color
        marker["color"] = color
    elif line:
        mode = "lines"
        line["color"] = color
    elif marker:
        mode = "markers"
        marker["color"] = color
    return mode, line, marker


def normalize_kwargs(format_string, **kwargs):
    mode, line, marker = convert_format_string_to_line2d_properties(format_string)
    plotly_kwargs = dict(mode=mode, line=line, marker=marker)
    for key, value in kwargs.items():
        if "color" == key:
            plotly_kwargs["line"]["color"] = "rgb" + str(tuple(value))
            plotly_kwargs["marker"]["color"] = "rgb" + str(tuple(value))
        if "label" == key:
            plotly_kwargs["name"] = value
        if "linestyle" == key:
            _line = convert_line_format(value)
            plotly_kwargs["line"]["dash"] = _line["dash"]
        if "linewidth" == key:
            plotly_kwargs["line"]["width"] = value
        if "marker" == key:
            _marker = convert_marker_format(value)
            plotly_kwargs["marker"]["symbol"] = _marker["symbol"]
        if "markersize" == key:
            plotly_kwargs["marker"]["size"] = value
    return plotly_kwargs


if __name__ == '__main__':
    pass
