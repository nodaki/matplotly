#! env python
# -*- coding: utf-8 -*-
# Date: 2019/04/21
# Filename: test_pyplot

import numpy as np


def plot_args(*args, **kwargs):
    print(args)
    print(True)


def test_plot_args():
    x1 = np.arange(5)
    x2 = np.arange(5, 10)
    y1 = np.arange(5)
    y2 = np.arange(5, 10)
    plot_args(x1, y1, x2, y2, x=x1)

if __name__ == '__main__':
    test_plot_args()
