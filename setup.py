from setuptools import setup

setup(
    name='matplotly',
    version='1.0.0',
    packages=['matplotly'],
    url='https://gitlab.com/nodaki/matplotly',
    author='akihiro',
    description='plotly wrapper with matplotlib-like API.',
    install_requires=['plotly']
)
